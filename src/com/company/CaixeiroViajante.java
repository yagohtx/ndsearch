package com.company;

import static java.lang.Math.min;

public class CaixeiroViajante {

    private boolean[] visited;
    private int nodes;


    public CaixeiroViajante(int vertices) {
        this.visited = new boolean[vertices];
        this.nodes = vertices;
        visited[0] = true;
    }

    public int start(int[][] graph, int currentPosition, int count, int aresta, int total) {

        if (count == nodes && graph[currentPosition][0] > 0) {
            total = min(total, aresta + graph[currentPosition][0]);
            return total;
        }
        for (int i = 0; i < nodes; i++) {
            if (!visited[i] && graph[currentPosition][i] > 0) {

                visited[i] = true;
                total = start(graph, i, count + 1,
                        aresta + graph[currentPosition][i], total);

                visited[i] = false;
            }
        }
        return total;
    }
}
