package com.company;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

public class DB {

    Logger logger = Logger.getLogger("logger");

    private int[] numbers;

    public DB(int[] numbers) {
        this.numbers = numbers;
    }

    public int[] getNumbers() {
        return numbers;
    }

    public void orderAscND(){
        int randomNum;
        int temp;
        try {
            for(int i = 0; i < numbers.length; i++){
                randomNum = ThreadLocalRandom.current().nextInt(i + 1, numbers.length);
                for (int j = 0; j <= i; j++) {
                    if (numbers[randomNum] < numbers[j]) {
                        temp = numbers[j];
                        numbers[j] = numbers[randomNum];
                        numbers[randomNum] = temp;
                    }
                }
            }
        } catch (Exception e){
            System.out.println("Successfully ordered");
        }
    }

    public void orderDescND() {
        int randomNum;
        int temp;
        try {
            for (int i = 0; i < numbers.length; i++) {
                randomNum = ThreadLocalRandom.current().nextInt(i + 1, numbers.length);
                for (int j = 0; j <= i; j++) {
                    if (numbers[randomNum] > numbers[j]) {
                        temp = numbers[j];
                        numbers[j] = numbers[randomNum];
                        numbers[randomNum] = temp;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Successfully ordered");
        }
    }
    public int findBiggestND(){
        int randomNum =  ThreadLocalRandom.current().nextInt(0, numbers.length);
        return numbers[randomNum];
    }

    public Search search(int number) {
        return new Search(number);
    }

    public class Search{

        private int search;
        private int index1;
        private int index2; // between

        private int[] excludedIndexes = new int[0];

        public Search(int search) {
            this.search = search;
        }

        public Search between(int index1, int index2){
            this.index1 = index1;
            this.index2 = index2;
            logger.info("Choose between: " + index1 + " || " + index2);
            return this;
        }

        public Search excludeIndex(int index){
            int arrLen = this.excludedIndexes.length;
            int[] newArr = new int[arrLen + 1];
            for(int i = 0; i < newArr.length; i++){
                try{
                    newArr[i] = this.excludedIndexes[i];
                } catch (Exception e){
                    newArr[i] = index;
                }
            }
            this.excludedIndexes = newArr;
            logger.info("Excluded indexes: " + Arrays.toString(this.excludedIndexes));
            return this;
        }

        public boolean run(){
            int randomNum =  ThreadLocalRandom.current().nextInt(this.index1, this.index2 + 1);
             while (isExcluded(randomNum)){
                 randomNum =  ThreadLocalRandom.current().nextInt(this.index1, this.index2);
             }

            return search == getNumbers()[randomNum];
        }

        private boolean isExcluded(int index) {
            if (this.excludedIndexes.length != 0) {
                for (int excludedIndex : this.excludedIndexes) {
                    if (index == excludedIndex) {
                        return true;
                    }
                }
                return false;
            }
            return false;
        }
    }
}
