package com.company;

import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {

        Logger logger = Logger.getLogger("D2");

        DB db = new DB(new int[] { 3, 5, 2, 10 ,5 ,6, 6, 9 });
        int number = 5;
        boolean result = db.search(number).between(1,4).run(); //Inclusive index!!

        logger.info("Searching for number " + number + "...");
        System.out.println(result ? "Found!" : "Not found...");

        System.out.println("Possible biggest number: " + db.findBiggestND());

        db.orderAscND();
        for(int n : db.getNumbers()){
            System.out.print(n + " - ");
        }

        System.out.println();

        db.orderDescND();
        for(int n : db.getNumbers()){
            System.out.print(n + " - ");
        }

        System.out.println();


        ///////////////////////////////////////////////////////////////////

        System.out.println("Ciclo de Hamilton: ");
        Hamilton hamilton = new Hamilton(5);

        // Exercice's graph

        //  0  ----  1
        // | \      /
        // |    4
        // | /     \
        //  3 ------ 2

        int[][] graph1 =
                {
                        //0  1  2  3  4
                        {0, 2, 0, 7, 4}, // 0
                        {6, 0, 0, 0, 2}, // 1
                        {0, 0, 0, 7, 4}, // 2
                        {3, 6, 10, 0, 2}, // 3
                        {2, 13, 8, 9, 0}, // 4
                };

        hamilton.cycle(graph1);

        ///////////////////////////////////////////////////////////////////

        System.out.println("Caixeiro Viajante: ");

        CaixeiroViajante caixeiroViajante = new CaixeiroViajante(4);

        int[][] graph2 = {
                {0, 10, 15, 20},
                {10, 0, 35, 25},
                {15, 35, 0, 30},
                {20, 25, 30, 0}
        };

        int ans = Integer.MAX_VALUE;
        ans = caixeiroViajante.start(graph2, 0, 1, 0, ans);


        System.out.println(ans);

    }
}

