package com.company;

public class Hamilton {

    public int vertices;
    public int[] path;

    public Hamilton(int vertices) {
        this.vertices = vertices;
    }

    boolean acceptable(int v, int[][] graph, int[] path, int pos) {
        if (graph[path[pos - 1]][v] == 0)
            return false;

        for (int i = 0; i < pos; i++)
            if (path[i] == v)
                return false;

        return true;
    }

    void cycle(int[][] graph) {
        path = new int[vertices];
        for (int i = 0; i < vertices; i++)
            path[i] = -1;

        path[0] = 0;
        if (!cycleHandler(graph, path, 1))
        {
            System.out.println("\nNo solution");
            return;
        }

        printSolution(path);
    }

    boolean cycleHandler(int[][] graph, int[] path, int pos) {
        if (pos == vertices)
        {
            return graph[path[pos - 1]][path[0]] == 1;
        }

        for (int v = 1; v < vertices; v++)
        {

            if (acceptable(v, graph, path, pos))
            {
                path[pos] = v;

                if (cycleHandler(graph, path, pos + 1))
                    return true;

                path[pos] = -1;
            }
        }

        return false;
    }



    void printSolution(int[] path) {
        System.out.println("Solution: ");
        for (int i = 0; i < vertices; i++)
            System.out.print(" " + path[i] + " ");

        System.out.println(" " + path[0] + " ");
    }
}
